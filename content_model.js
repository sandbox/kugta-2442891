(function ($) {
  Drupal.behaviors.content_model = {
    attach: function(context) {
      var graph = Drupal.settings.content_model.graph;

      var defaultR = 8;
      var margin = {top: -5, right: -5, bottom: -5, left: -5};
      var width = 1200 - margin.left - margin.right,
          height = 500- margin.top - margin.bottom;

      var color = d3.scale.category20();

      var force = d3.layout.force()
        .theta(1)
        .charge(function(d) { return -200; })
        .linkDistance(function (d) {return 60 + (d.source.weight * 5);})
        .size([width + margin.left + margin.right, height + margin.top + margin.bottom]);

      var zoom = d3.behavior.zoom()
            .scaleExtent([1, 10])
            .on("zoom", zoomed);

      var drag = d3.behavior.drag()
            .origin(function(d) { return d; })
            .on("dragstart", dragstarted)
            .on("drag", dragged)
            .on("dragend", dragended);


      var svg = d3.select("#content_model_graph").append("svg")
            .attr("class", "content_model")
            .attr("viewBox", "0 0 " + (width + margin.left + margin.right) + " " + (height + margin.top + margin.bottom))
            .attr("preserveAspectRatio", "xMidYMid meet")
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
            .call(zoom);

      svg.append("defs").selectAll("marker")
          .data(["suit", "licensing", "resolved"])
        .enter().append("marker")
          .attr("id", function(d) { return d; })
          .attr("viewBox", "0 -5 10 10")
          .attr("refX", 22)
          .attr("refY", 0)
          .attr("markerWidth", 6)
          .attr("markerHeight", 6)
          .attr("orient", "auto")
        .append("path")
          .attr("d", "M0,-5L10,0L0,5 L10,0 L0, -5");

      var rect = svg.append("rect")
            .attr("width", width)
            .attr("height", height)
            .style("fill", "none")
            .style("pointer-events", "all");

      var container = svg.append("g");

      force
        .nodes(graph.nodes)
        .links(graph.links)
        .start();

      var legendIndex = 0;
      var entityTypes = [];

      graph.entiy_types.forEach(function(d) {
        var entity_type = {};
        entity_type.name = d;
        entity_type.index = legendIndex;
        entityTypes.push(entity_type);
        legendIndex++;
      });

      var legend = container.append("g")
            .attr("class", "legends")
            .selectAll(".legend")
            .data(entityTypes)
              .enter().append("g")
                .attr("class", "legend");

          legend.append("rect")
            .attr("width", 5)
            .attr("height", 15)
            .attr("x", 10)
            .attr("y", function(d) {
              legendIndex++;
              return (d.index * 25) + 5; }
            )
            .style("fill", function(d) { return color(d.name); })
            .style("pointer-events", "all");

          legend.append("text")
            .attr("x", 20)
            .attr("y", function(d) {
              legendIndex++;
              return (d.index * 25) + 17; }
            )
            .text(function(d) {
              return cleanstring(d.name);;
            });

      var link = container.append("g")
            .attr("class", "links")
            .selectAll(".link")
            .data(graph.links)
              .enter().append("line")
                .attr("class", "link")
                //.style("marker-end",  "url(#resolved)")
                .style("stroke-width", function(d) { return 1; });

      var node = container.append("g")
            .attr("class", "nodes")
            .selectAll(".node")
            .data(graph.nodes)
              .enter().append("g")
                .attr("class", function(d) { return d.connected ? 'node connected' : 'node unconnected'; })
                .attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; })
                .call(drag);

      node.append("text")
        .attr("class", "info-label")
        .attr("x", function(d) { return getR(d) + 3; })
        .attr("y", function(d) { return getR(d) + 3; })
        .text(function(d) {
          return cleanstring(d.name);
        })
        .style("stroke-width", function(d) { return 0; });

      node.append("circle")
        .attr("r", getR)
        .style("fill", function(d) { return color(d.entity_type); });

      force.on("tick", function() {
        link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

        node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
      });

      var linkedByIndex = {};
      graph.links.forEach(function(d) {
          linkedByIndex[d.source.index + "," + d.target.index] = 1;
      });

      function isConnected(a, b) {
          return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index];
      }

      node
      .on("mouseover", function(d){
        node.classed("node-active", function(o) {
          return isConnected(d, o);
        });

        node.classed("node-dim", function(o) {
          return !isConnected(d, o) && d != o;
        });

        link.classed("link-active", function(o) {
          return o.source === d || o.target === d ? true : false;
        });

        link.classed("link-dim", function(o) {
          return o.source === d || o.target === d ? false : true;
        });

        d3.select(this).classed("node-active", true);
        d3.select(this).classed("node-highlight", true);
        d3.select(this).select("circle").transition()
          .duration(750)
          .attr("r", getR(d) * 1.2);
      })
      .on("mouseout", function(d){
        node.classed("node-highlight", false);
        node.classed("node-dim", false);
        node.classed("node-active", false);
        link.classed("link-active", false);
        link.classed("link-dim", false);

        d3.select(this).select("circle").transition()
          .duration(750)
          .attr("r", getR(d));
      });

      function cleanstring(s) {
        return s.replace(new RegExp("_", "g"), ' ')
      }

      function getR(d) {
        return defaultR + d.weight * 2;
      }

      function dottype(d) {
        d.x = +d.x;
        d.y = +d.y;
        return d;
      }

      function zoomed() {
        container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
      }

      function dragstarted(d) {
        d3.event.sourceEvent.stopPropagation();
        d3.select(this).classed("dragging", true);
        force.start();
      }

      function dragged(d) {
        d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);

      }

      function dragended(d) {
        d3.select(this).classed("dragging", false);
      }
    }
  }
})(jQuery);
